from .models import *
from django import forms
from django.conf import settings
import datetime
from datetime import datetime, time, date, timedelta
from dal import autocomplete

min_date = date.today() - timedelta(50*365)
today = date.today() - timedelta(18*365)

class PhotographerAPForm(forms.ModelForm):

    birth_date = forms.DateField(widget=forms.DateTimeInput(format=('%Y-%m-%d'), attrs={'class':'form-control datepicker', 'min':min_date ,'max': today, 'value': today,  'type':'date' } ), 
                      )

    class Meta:
        model = PhotographerAP
        fields = '__all__'
        exclude = ['user', 'is_active']


        widgets = {
            'language' : autocomplete.ModelSelect2Multiple(url='client:languague-autocomplete'),
            'city': autocomplete.ModelSelect2(url='client:location-autocomplete'),
            'public_bio': forms.Textarea(attrs={"rows":5, "cols":20}),
            'youtube': forms.TextInput(attrs={'placeholder': '@amplypix, recommended format' }),
            'instagram': forms.TextInput(attrs={'placeholder': '@amplypix, recommended format'}),
            'tiktok': forms.TextInput(attrs={'placeholder': '@amplypix, recommended format'}),
        }

class BriefcaseForm(forms.ModelForm):

    video_url = forms.URLField()

    class Meta:
        model = Briefcase
        fields = '__all__'
        exclude = ['user']
