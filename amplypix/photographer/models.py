from django.db import models
from modrator.models import User, Location, Languague
from client.validators import validate_file_size
from client.categories import *


class PhotographerAP(models.Model):

    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    public_bio = models.CharField(max_length=150, blank=True, null=True)
    profile_picture = models.ImageField(upload_to='client/profile/avatar/%Y/%m/%d', default='client/profile/default.jpg', null=True, blank=True, validators=[validate_file_size])
    
    
    city = models.ForeignKey(Location, on_delete=models.CASCADE, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    language = models.ManyToManyField(Languague, blank=True)

    is_active = models.BooleanField(default = False)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    #WEBSITES & SOCIAL CHANNEL
    youtube = models.CharField(max_length=50, blank=True, null=True)
    instagram = models.CharField(max_length=60, blank=True, null=True)
    tiktok = models.CharField(max_length=60, blank=True, null=True)

    def validate_briefcase(self):
        s = Briefcase.objects.get(user=self)
        return str(s)

class Briefcase(models.Model):

    user = models.OneToOneField(PhotographerAP, on_delete=models.CASCADE, primary_key=True)
    photo_1 = models.ImageField(upload_to='client/profile/briefcase/%Y/%m/%d', validators=[validate_file_size])
    photo_2 = models.ImageField(upload_to='client/profile/briefcase/%Y/%m/%d', validators=[validate_file_size])
    photo_3 = models.ImageField(upload_to='client/profile/briefcase/%Y/%m/%d', validators=[validate_file_size])
    photo_4 = models.ImageField(upload_to='client/profile/briefcase/%Y/%m/%d', validators=[validate_file_size])
    photo_5 = models.ImageField(upload_to='client/profile/briefcase/%Y/%m/%d', validators=[validate_file_size])
    video_url = models.CharField(max_length=150, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
