from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
import os
from django.http import JsonResponse
import datetime
from datetime import datetime, time, date, timedelta
from django.views.generic import TemplateView
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required
import urllib, json
import urllib.request
from django.contrib.contenttypes.models import ContentType
from urllib.request import Request, urlopen
from .models import *
from .forms import *
from modrator.models import User, Location
from modrator.forms import UserUpdateForm
from dal import autocomplete
# Create your views here.


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_photographer, login_url='client:index')
def complete_profile_photographer(request):

    photographer = PhotographerAP.objects.get(user=request.user)

    if request.method == 'POST':
        form = PhotographerAPForm(request.POST, request.FILES, instance=photographer)

        if form.is_valid():

            y = form.save(commit = False)
            y.save()
            form.save_m2m()
            messages.success(request, 'Your profile has been successfully updated!')
            return redirect('photographer:complete_profile_briefcase')

    else :
        form = PhotographerAPForm(instance=photographer)

    return render(request, 'photographer/index.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_photographer, login_url='client:index')
def complete_profile_briefcase(request):

    photographer = PhotographerAP.objects.get(user=request.user)

    if request.method == 'POST':
        form = BriefcaseForm(request.POST, request.FILES)

        if form.is_valid():

            y = form.save(commit = False)
            y.user = photographer
            y.save()
            messages.success(request, 'Your briefcase has been successfully updated!')
            return redirect('client:index')

    else :
        form = BriefcaseForm()

    return render(request, 'photographer/briefcase.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_photographer, login_url='client:index')
def dashboard_photographer(request):

    photographer = PhotographerAP.objects.get(user=request.user)

    return render(request, 'photographer/dashboard.html', locals())

@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_photographer, login_url='client:index')
def update_profile_photographer(request):

    photographer = PhotographerAP.objects.get(user=request.user)
    update_complete = photographer

    if request.method == 'POST':
        form = PhotographerAPForm(request.POST, request.FILES, instance=photographer)

        if form.is_valid():

            y = form.save(commit = False)
            y.save()
            form.save_m2m()
            messages.success(request, 'Your profile has been successfully updated!')
            return redirect('photographer:dashboard_photographer')

    else :
        form = PhotographerAPForm(instance=photographer)

    return render(request, 'photographer/index.html', locals())

@login_required(redirect_field_name='/')
def profile_photographer(request, username):

    user = User.objects.get(username=username)
    photographer = PhotographerAP.objects.get(user=user)
    briefcase_ = Briefcase.objects.filter(user=photographer).first()

    return render(request, 'photographer/profile/profile.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_photographer, login_url='client:index')
def update_briefcase(request):

    photographer = PhotographerAP.objects.get(user=request.user)
    briefcase__ = Briefcase.objects.get(user=photographer)

    if request.method == 'POST':
        form = BriefcaseForm(request.POST, request.FILES, instance=briefcase__)

        if form.is_valid():

            y = form.save(commit = False)
            y.user = photographer
            y.save()
            messages.success(request, 'Your briefcase has been successfully updated!')
            return redirect('photographer:profile_photographer', request.user)

    else :
        form = BriefcaseForm(instance=briefcase__)

    return render(request, 'photographer/briefcase.html', locals())