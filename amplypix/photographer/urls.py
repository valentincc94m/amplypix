from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from . import views
from .views import *

app_name = "photographer"

urlpatterns = [
    
    # complete profile photographer
    path('complete_profile_photographer', complete_profile_photographer, name='complete_profile_photographer'),

    # briefcase  
    path('complete_profile_briefcase', complete_profile_briefcase, name='complete_profile_briefcase'),
    # Dashboard Photographer
    path('dashboard_photographer', dashboard_photographer, name='dashboard_photographer'),
    # Update Profile
    path('update_profile_photographer', update_profile_photographer, name='update_profile_photographer'),
    # Profile 
    url(r'^profile_photographer/(?P<username>\w+|[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$', profile_photographer, name='profile_photographer'),
    # Update Briefcase
    path('update_briefcase', update_briefcase, name='update_briefcase'),
] 