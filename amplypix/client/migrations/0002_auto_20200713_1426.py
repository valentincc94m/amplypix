# Generated by Django 3.0.7 on 2020-07-13 14:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientap',
            name='short_bio',
            field=models.CharField(blank=True, max_length=150, null=True),
        ),
        migrations.AlterField(
            model_name='clientap',
            name='type_client',
            field=models.CharField(blank=True, choices=[('Individual', 'Individual'), ('Agency / Company', 'Agency / Company'), ('Media Company', 'Media Company'), ('Non Profit / NGO', 'Non Profit / NGO'), ('Other', 'Other')], max_length=20, null=True),
        ),
    ]
