interes_list = (
    ('Facebook, Inc', 'Facebook, Inc'),
    ('Adobe', 'Adobe'),
    ('Instagram', 'Instagram'),
    ('Fashion', 'Fashion'),
    ('Wedding', 'Wedding'),
    ('Nature', 'Nature')
)

price_type = (
    ('$ Dolar', '$ Dolar'),
    ('₹ Rupees', '₹ Rupees')
)

states = (
    ('Applied', 'Applied'),
    ('Viewed', 'Viewed'),
    ('In process', 'In process'),
    ('Selected', 'Selected')
)

sex = (
    ('Male', 'Male'),
    ('Female', 'Female'),
    ('Other', 'Other')
)

type_c = (
    ('Individual', 'Individual'),
    ('Agency / Company', 'Agency / Company'),
    ('Media Company', 'Media Company'),
    ('Non Profit / NGO', 'Non Profit / NGO'),
    ('Other', 'Other')
)

category_task = (
    ('Wedding photography', 'Wedding photography'),
    ('Row photo edit', 'Row photo edit'),
    ('Print footage', 'Print footage'),
    ('Create video from phots', 'Create video from phots'),
    ('Create photo album (hardcopy)', 'Create photo album (hardcopy)')
)

status_task = (
    (u'Active',u'Active'),
    (u'Paused',u'Paused'),
    (u'Closed',u'Closed'),
)

photographer_status = (
    ('Applied', 'Applied'),
    ('Viewed', 'Viewed'),
    ('Finalist', 'Finalist'),
)