from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from . import views
from .views import *
from django.contrib.auth.views import LoginView

app_name = "client"

urlpatterns = [
    

    #index / login
    path('', LoginView.as_view(redirect_authenticated_user=True), name="login"),
    # Index Redirect Client / Photographer
    path('index', index, name='index'),


    # Complete Profile
    path('complete_profile_client', complete_profile_client, name='complete_profile_client'),
    # Dashboard Client
    path('update_profile', update_profile, name='update_profile'),
    # Profile Client
    url(r'^profile_client/(?P<username>\w+|[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$', profile_client, name='profile_client'),
    # Update Profile Client
    path('dashboard_client', dashboard_client, name='dashboard_client'),
    # Post Task
    path('post_task', post_task, name='post_task'),
    # Edit Task
    url(r'^edit_task/(?P<id>\d+)/$', edit_task, name='edit_task'),
    # List Task 
    path('list_task', list_task, name='list_task'),

    url(
        r'^location-autocomplete/$',
        LocationAutocomplete.as_view(),
        name='location-autocomplete',
    ),


    url(
        r'^languague-autocomplete/$',
        LanguagueAutocomplete.as_view(),
        name='languague-autocomplete',
    ),


] 