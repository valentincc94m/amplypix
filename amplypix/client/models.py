from django.db import models
from modrator.models import User, Location
from .validators import validate_file_size
from .categories import type_c, category_task, status_task, photographer_status
from photographer.models import PhotographerAP

class ClientAP(models.Model):

    profile_picture = models.ImageField(upload_to='client/profile/avatar/%Y/%m/%d', default='client/profile/default.jpg', null=True, blank=True, validators=[validate_file_size])
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    is_active = models.BooleanField(default = False)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    type_client = models.CharField(max_length=20, choices=type_c, blank=True, null=True)
    company_name = models.CharField(max_length=100)
    website = models.CharField(max_length=50, blank=True, null=True)
    short_bio = models.CharField(max_length=150, blank=True, null=True)
    city = models.ForeignKey(Location, on_delete=models.CASCADE, blank=True, null=True)

class Task(models.Model):  

    user = models.ForeignKey(ClientAP, on_delete=models.CASCADE)
    title =  models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default = False)
    description = models.CharField(max_length=250)
    budget = models.CharField(max_length=7)
    category = models.CharField(max_length=31, choices=category_task)
    keywords = models.CharField(max_length=150)
    attach_file = models.FileField(upload_to="client/files", null=True, blank=True)
    applicants = models.ManyToManyField('client.evaluation_photographer', blank=True)
    status = models.CharField(max_length=30, choices=status_task, default='Active')


class Evaluation_photographer(models.Model):

    tasks = models.ForeignKey(Task, on_delete=models.CASCADE)
    photographer = models.ForeignKey('photographer.PhotographerAP', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=30, choices=photographer_status, default='Applied')