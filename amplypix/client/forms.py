from .models import *
from django import forms
from django.conf import settings
import datetime
from datetime import datetime, time, date, timedelta
from dal import autocomplete

min_date = date.today() - timedelta(50*365)
today = date.today() - timedelta(18*365)

class ClientAPForm(forms.ModelForm):

    website = forms.URLField()

    class Meta:
        model = ClientAP
        fields = '__all__'
        exclude = ['user', 'is_active']

        widgets = {
            'short_bio': forms.Textarea(attrs={"rows":5, "cols":20}),
            'city': autocomplete.ModelSelect2(url='client:location-autocomplete'),
        }

class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        exclude = ['applicants', 'is_active']

