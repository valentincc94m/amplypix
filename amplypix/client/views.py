from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
import os
from django.http import JsonResponse
import datetime
from datetime import datetime, time, date, timedelta
from django.views.generic import TemplateView
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required
import urllib, json
import urllib.request
from django.contrib.contenttypes.models import ContentType
from urllib.request import Request, urlopen
from .models import *
from .forms import *
from modrator.models import User, Location, Languague
from modrator.forms import UserUpdateForm
from dal import autocomplete
from photographer.models import PhotographerAP, Briefcase
from modrator.forms import UserUpdateForm

# index.
@login_required(redirect_field_name='/')
def index(request):

    if request.user.is_photographer:
        photographer = PhotographerAP.objects.get(user=request.user)
        briefcase = Briefcase.objects.filter(user=photographer).first()

        if briefcase:
            return redirect('photographer:dashboard_photographer')
        else: 
            return redirect('photographer:complete_profile_photographer')

    
    elif request.user.is_client:
        client = ClientAP.objects.get(user=request.user)
        if client.type_client == None:
            return redirect('client:complete_profile_client')
        else:   
            return redirect('client:dashboard_client')

    return render(request, 'client/index.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_client, login_url='client:index')
def complete_profile_client(request):
    client = ClientAP.objects.get(user=request.user)

    if request.method == 'POST':
        form = ClientAPForm(request.POST, request.FILES, instance=client)

        if form.is_valid():

            y = form.save(commit = False)
            y.save()
            form.save_m2m()
            messages.success(request, 'Your profile has been successfully updated!')
            return redirect('photographer:complete_profile_briefcase')

    else :
        form = ClientAPForm(instance=client)

    return render(request, 'client/complete/index.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_client, login_url='client:index')
def update_profile(request):

    client_f = ClientAP.objects.get(user=request.user)
    user = User.objects.get(username=request.user.username)

    if request.method == 'POST':
        form = ClientAPForm(request.POST, request.FILES, instance = client_f)
        form_two = UserUpdateForm(request.POST, instance=user)

        if form.is_valid() and form_two.is_valid():

            y = form.save(commit = False)
            y.save()
            t = form_two.save(commit=False)
            t.save()
            messages.success(request, 'Your profile has been successfully updated!')
            return redirect('client:profile_client', request.user)

    else :
        form = ClientAPForm(instance = client_f)
        form_two = UserUpdateForm(instance=user)
    return render(request, 'client/edit/profile.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_client, login_url='client:index')
def dashboard_client(request):

    client = ClientAP.objects.get(user=request.user)

    return render(request, 'client/dashboard.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_client, login_url='client:index')
def post_task(request):

    client = ClientAP.objects.get(user=request.user)

    if request.method == 'POST':
        form = TaskForm(request.POST, request.FILES)

        if form.is_valid():

            y = form.save(commit = False)
            y.user = client
            y.save()
            form.save_m2m()
            messages.success(request, 'Your task has been published successfully!')
            return redirect('client:list_task')

    else :
        form = TaskForm()

    return render(request, 'client/task/add.html', locals())


@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_client, login_url='client:index')
def edit_task(request, id):

    client = ClientAP.objects.get(user=request.user)
    task = Task.objects.get(id=id)

    if request.method == 'POST':
        form = TaskForm(request.POST, request.FILES, instance=task)

        if form.is_valid():

            y = form.save(commit = False)
            y.user = client
            y.save()
            form.save_m2m()
            messages.success(request, 'Your task has been updated successfully!')
            return redirect('client:list_task')

    else :
        form = TaskForm(instance=task)

    return render(request, 'client/task/add.html', locals())

@login_required(redirect_field_name='/')
@user_passes_test(lambda u: u.is_client, login_url='client:index')
def list_task(request):

    client = ClientAP.objects.get(user=request.user)
    task = Task.objects.filter(user=client)

    return render(request, 'client/task/list.html', locals())


def profile_client(request, username):

    user = User.objects.get(username=username)

    client = ClientAP.objects.get(user=user)


    return render(request, 'client/profile.html', locals())


class LocationAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Location.objects.none()

        qs = Location.objects.all().order_by('name')

        if self.q:
          qs = qs.filter(name__istartswith=self.q)

        
        return qs


class LanguagueAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Languague.objects.none()

        qs = Languague.objects.all().order_by('name')

        if self.q:
          qs = qs.filter(name__istartswith=self.q)

        
        return qs

