from django.db import models
from django.contrib.auth.models import AbstractUser
from phone_field import PhoneField

# Create your models here.
class User(AbstractUser):
    is_client = models.BooleanField(default = False)
    is_photographer = models.BooleanField(default = False)
    is_modrator = models.BooleanField(default = False)
    email = models.EmailField(unique = True)
    phone = PhoneField(blank=True, unique=True)

class Location(models.Model):
    name = models.CharField(max_length=100)
    state = models.CharField(max_length=100)

    def __str__(self):
        s = str(self.name) + " , " + str(self.state)
        return (s)


class Languague(models.Model):
    name = models.CharField(max_length=100)
    nativeName = models.CharField(max_length=100)

    def __str__(self):
        s = str(self.name)
        return (s)