from django.urls import path, include
from django.conf import settings
from django.conf.urls import url
from . import views
from .views import *

app_name = "modrator"

urlpatterns = [
    
    # signup photographer
    path('signup_photographer', signup_photographer, name='signup_photographer'),
    # signup client
    path('signup_client', signup_client, name='signup_client'),




] 