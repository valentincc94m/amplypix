from django.apps import AppConfig


class ModratorConfig(AppConfig):
    name = 'modrator'
