from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from .models import *
from .forms import *
from client.models import ClientAP
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash
import datetime
from datetime import datetime, time, date, timedelta
import json
from django.core.serializers import serialize 
from django.http import JsonResponse
from photographer.models import PhotographerAP
from client.models import ClientAP

def signup_photographer(request):

    if request.method == 'POST':
        form = UCFWithEmail(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.email = str(user.username)
            user.is_photographer = True
            user.save()
            client = PhotographerAP.objects.create(user=user)
            messages.success(request, 'Your Account has been created successfull!')
            login(request, user)
            return redirect('client:index')
    else:
        form = UCFWithEmail()

    return render(request, 'registration/signup_photographer.html', locals())


def signup_client(request):

    if request.method == 'POST':
        form = UCFWithEmail(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.email = str(user.username)
            user.is_client = True
            user.save()
            client = ClientAP.objects.create(user=user)
            messages.success(request, 'Your Account has been created successfull!')
            login(request, user)
            return redirect('client:index')
    else:
        form = UCFWithEmail()

    return render(request, 'registration/signup_client.html', locals())