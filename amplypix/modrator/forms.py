from .models import User
from django import forms
from django.contrib.auth.forms import AuthenticationForm,PasswordResetForm, UserCreationForm
from django.conf import settings

# Extendemos del original
class UCFWithEmail(UserCreationForm):
    # Ahora el campo username es de tipo email y cambiamos su texto
    username = forms.EmailField(label="Email address")
    first_name = forms.CharField(max_length=30, label="First Name")
    last_name = forms.CharField(max_length=30, label="last Name")

    class Meta:
        model = User
        fields = ["username", "password1", "password2", 'first_name','last_name', "phone"]
        exclude = ['is_client', 'is_modrator', 'is_photographer']

class UserUpdateForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'phone']
        exclude = ['username']