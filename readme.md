steps to run this project

1) create a viru environment: 

preferably call it amply-env since this is how it is in the 
.gitignore in case they are going to create it inside the git folder

command-line:

Linux and OS X:

python3 -m venv amply-env 

Windows:
 
python -m venv myvenv

once executed, just execute the following command to install the libraries

2) Install libraries

command-line:

Linux and OS X:

pip3 install -r requirements.txt

Windows:

pip install -r requirements.txt

3) finalmence run 

command-line:

python manage.py runserver 

or 

python3 manage.py runserver
